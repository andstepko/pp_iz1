// kr1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <cstdlib>
#include <limits>
#include "windows.h"
#include <time.h>
#include <omp.h>
#include <float.h>

using namespace std;

//float** matrix;
//float** matrix2;
//float* vector;

void printSeparator()
{
	printf("------------------------------------\n");
}

void printMatrix(float** matrix, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			printf("%0.2f ", matrix[i][j]);
		}
		printf("\n");
	}
	printSeparator();
}

float* generateRandomVector(int length)
{
	float* vector = 0;
	vector = new float[length];

	for(int i = 0; i < length; i++)
	{
		vector[i] = rand() % 5;
	}

	return vector;
}

float** generateRandomMatrix(int n)
{
	float** matrix = (float**)malloc(n*sizeof(float*));

	for(int i = 0; i < n; i++)
	{
		matrix[i] = (float*)malloc(n * sizeof(float));
		for(int j = 0; j < n; j++)
		{
			matrix[i][j] = rand() % 5;
		}
	}

	return matrix;
}

float** fillMatrix44()
{
	float** matrix = (float**) calloc(4, sizeof(float*));

	matrix[0] = (float*)malloc(4*sizeof(float));
	matrix[0][0] = 0;
	matrix[0][1] = 1;
	matrix[0][2] = 2;
	matrix[0][3] = 3;

	matrix[1] = (float*)malloc(4*sizeof(float));
	matrix[1][0] = 4;
	matrix[1][1] = 5;
	matrix[1][2] = 6;
	matrix[1][3] = 7;

	matrix[2] = (float*)malloc(4*sizeof(float));
	matrix[2][0] = 8;
	matrix[2][1] = 9;
	matrix[2][2] = 10;
	matrix[2][3] = 11;

	matrix[3] = (float*)malloc(4*sizeof(float));
	matrix[3][0] = 12;
	matrix[3][1] = 13;
	matrix[3][2] = 14;
	matrix[3][3] = 15;
	return matrix;
}

float findMax(float** matrix, int n, int &resultHeight, int &resultWidth)
{
	resultHeight = 0;
	resultWidth = 0;
	
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(matrix[i][j] > matrix[resultHeight][resultWidth])
			{
				resultHeight = i;
				resultWidth = j;
			}
		}
	}

	return matrix[resultHeight][resultWidth];
}

//float** clone(float** matrix, int n)
//{
//	float** result = (float**)malloc(n*sizeof(float*));
//
//	for(int i = 0; i < n; i++)
//	{
//		result[i] = (float*)malloc(n * sizeof(float));
//		for(int j = 0; j < n; j++)
//		{
//			result[i][j] = matrix[i][j];
//		}
//	}
//	return result;
//}

bool equals(float** matrix, float** matrix2, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			if(((matrix[i][j] - matrix2[i][j]) > 10) || ((matrix[i][j] - matrix2[i][j]) < -10))
			//if(matrix[i][j] != matrix2[i][j])
			{
				return false;
			}
		}
	}

	return true;
}

float* multiply(float** matrix, int h, int w, float* vector)
{
	float* result = 0;
	result = new float[h];

	float accum;

	for(int i = 0; i < h; i++)
	{
		accum = 0;
		for(int j = 0; j < w; j++)
		{
			accum += matrix[i][j] * vector[j];
		}
		result[i] = accum;
	}

	return result;
}

float** stupidMultiply(float** matrix, float** matrix2, int n)
{
	float** result = (float**)malloc(n*sizeof(float*));

	for(int i = 0; i < n; i++)
	{
		result[i] = (float*)malloc(n*sizeof(float));
		for(int j = 0; j < n; j++)
		{
			for(int k = 0; k < n; k++)
			{
				result[i][j] += matrix[i][k] * matrix2[k][j];
			}
		}
	}
	return result;
}

float** stupidMultiply(float** a, float** b, int n, int hStartA, int wStartA, int hStartB, int wStartB)
{
	float** result = (float**)malloc(n*sizeof(float*));

	for(int i = 0; i < n; i++)
	{
		result[i] = (float*)calloc(n, sizeof(float));
		for(int j = 0; j < n; j++)
		{
			for(int k = 0; k < n; k++)
			{
				result[i][j] += a[hStartA + i][wStartA + k] * b[hStartB + k][wStartB +j];
			}
		}
	}
	return result;
}

float** notAstupidMultiply(float** a, float** b, int n, int hStartA, int wStartA, int hStartB, int wStartB)
{
	float** result = (float**)malloc(n*sizeof(float*));

	for(int i = 0; i < n; i++)
	{
		result[i] = (float*)calloc(n, sizeof(float));
		for(int j = 0; j < n; j++)
		{
			float t = a[i][j];
			for(int k = 0; k < n; k++)
			{
				result[i][k] += t * b[hStartB + j][wStartB +k];
			}
		}
	}
	return result;
}


//SHTRASSEN
// a - first matrix
// b - second matrix
float** sum(float** a, float** b, int n, int hStartA, int wStartA, int hStartB, int wStartB)
{
	float** result = (float**)calloc(n/2, sizeof(float*));

	for(int i = 0; i < n/2; i++)
	{
		result[i] = (float*)calloc(n/2, sizeof(float));
		for(int j = 0; j < n/2; j++)
		{
			result[i][j] = a[hStartA + i][wStartA + j] + b[hStartB + i][wStartB + j];
		}
	}
	return result;
}

// a - first matrix
// b - second matrix
float** substract(float** a, float** b, int n, int hStartA, int wStartA, int hStartB, int wStartB)
{
	float** result = (float**)malloc(n/2 * sizeof(float*));

	for(int i = 0; i < n/2; i++)
	{
		result[i] = (float*)malloc(n/2 * sizeof(float));
		for(int j = 0; j < n/2; j++)
		{
			result[i][j] = a[hStartA + i][wStartA + j] - b[hStartB + i][wStartB + j];
		}
	}
	return result;
}

//stacking functions
float** sumStuck(float** matrix, float** matrix2, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			matrix[i][j] += matrix2[i][j];
		}
	}
	return matrix;
}

float** substractStuck(float** matrix, float** matrix2, int n)
{
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			matrix[i][j] -= matrix2[i][j];
		}
	}
	return matrix;
}

//n - size of each quater
float** buildFromQuaters(float** q1, float** q2, float** q3, float** q4, int n)
{
	float** result = (float**)malloc((2*n)*sizeof(float*));

	//11
	for(int i = 0; i < n; i++)
	{
		result[i] = (float*)malloc(n*2*sizeof(float));
		for(int j = 0; j < n; j++)
		{
			result[i][j] = q1[i][j];
		}
	}
	//12
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			result[i][n + j] = q2[i][j];
		}
	}
	//21
	for(int i = 0; i < n; i++)
	{
		result[n + i] = (float*)malloc(n*2*sizeof(float));
		for(int j = 0; j < n; j++)
		{
			result[n + i][j] = q3[i][j];
		}
	}
	//22
	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < n; j++)
		{
			result[n + i][n + j] = q4[i][j];
		}
	}

	return result;
}

//n - size of a and b matrixes
float** pureShtrassenMultiply(float** a, float** b, int n, int hStartA, int wStartA, int hStartB, int wStartB)
{
	if(n <=128)
	{
		return notAstupidMultiply(a, b, n, hStartA, wStartA, hStartB, wStartB);
	}

	//sizes of P-s is n/2
	float** summa1 = sum(a, a, n, hStartA, wStartA, hStartA + n/2, wStartA + n/2);
	float** summa2 = sum(b, b, n, hStartB, wStartB, hStartB + n/2, wStartB + n/2);
	float** p1 = pureShtrassenMultiply(summa1, summa2, n/2, 0, 0, 0, 0);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);
	for(int i = 0; i < n/2; i++)
	{
		free(summa2[i]);
	}
	free(summa2);

	summa1 = sum(a, a, n, hStartA + n/2, wStartA, hStartA + n/2, wStartA + n/2);
	float** p2 = pureShtrassenMultiply(summa1, b, n/2, 0, 0, hStartB, wStartB);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);

	summa1 = substract(b, b, n, hStartB, wStartB + n/2, hStartB + n/2, wStartB + n/2);
	float** p3 = pureShtrassenMultiply(a, summa1, n/2, hStartA, wStartA, 0, 0);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);

	summa1 = substract(b, b, n, hStartB + n/2, wStartB, hStartB, hStartB);
	float** p4 = pureShtrassenMultiply(a, summa1, n/2, hStartA + n/2, wStartA + n/2, 0, 0);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);

	summa1 = sum(a, a, n, hStartA, wStartA, hStartA , wStartA + n/2);
	float** p5 = pureShtrassenMultiply(summa1, b, n/2, 0, 0, hStartB + n/2, wStartB + n/2);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);

	summa1 = substract(a, a, n, hStartA + n/2, wStartA, hStartA, wStartA);
	summa2 = sum(b, b, n, hStartB, wStartB, hStartB, wStartB + n/2);
	float** p6 = pureShtrassenMultiply(summa1, summa2, n/2, 0, 0, 0, 0);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);
	for(int i = 0; i < n/2; i++)
	{
		free(summa2[i]);
	}
	free(summa2);

	summa1 = substract(a, a, n, hStartA, wStartA + n/2, hStartA + n/2, wStartA + n/2);
	summa2 = sum(b, b, n, hStartB + n/2, wStartB, hStartB + n/2, wStartB + n/2);
	float** p7 = pureShtrassenMultiply(summa1, summa2, n/2, 0, 0, 0, 0);
	for(int i = 0; i < n/2; i++)
	{
		free(summa1[i]);
	}
	free(summa1);
	for(int i = 0; i < n/2; i++)
	{
		free(summa2[i]);
	}
	free(summa2);

	/*printf("p1 before:\n");
	printMatrix(p1, n/2);*/

	//C11 = p7
	sumStuck(p7, p4, n/2);
	substractStuck(p7, p5, n/2);
	sumStuck(p7, p1, n/2);
	//C12 = p5
	sumStuck(p5, p3, n/2);
	//C21 = p4
	sumStuck(p4, p2, n/2);
	//C22 = p1
	substractStuck(p1, p2, n/2);
	sumStuck(p1, p3, n/2);
	sumStuck(p1, p6, n/2);

	float** result = buildFromQuaters(p7, p5, p4, p1, n/2);

	for(int i = 0; i < n/2; i++)
	{
		free(p1[i]);
	}
	free(p1);
	for(int i = 0; i < n/2; i++)
	{
		free(p2[i]);
	}
	free(p2);
	for(int i = 0; i < n/2; i++)
	{
		free(p3[i]);
	}
	free(p3);
	for(int i = 0; i < n/2; i++)
	{
		free(p4[i]);
	}
	free(p4);
	for(int i = 0; i < n/2; i++)
	{
		free(p5[i]);
	}
	free(p5);
	for(int i = 0; i < n/2; i++)
	{
		free(p6[i]);
	}
	free(p6);
	for(int i = 0; i < n/2; i++)
	{
		free(p7[i]);
	}
	free(p7);

	return result;
}

int _tmain(int argc, _TCHAR* argv[])
{
	int N = 65536;
	int TESTS_NUMBER = 10;

	__int64 frequency;
	QueryPerformanceFrequency((LARGE_INTEGER *)&frequency);
	printf("frequency.Low==>%i\n", frequency);
	printSeparator();

	for(int i = 64; i < N; i*=2)
	{
		printf("Matrix size==>%i\n", i);

		float** matrix1 = generateRandomMatrix(i);
		float** matrix2 = generateRandomMatrix(i);
		float** stupMult;
		float** shtrasMult;

		__int64 start = 0, finish = 0;
		__int64 diff = MAXINT64;
		__int64 diff2 = MAXINT64;
		//Simple
        for (size_t t = 0; t < TESTS_NUMBER; t++) {
				QueryPerformanceCounter((LARGE_INTEGER *)&start);
				stupMult = notAstupidMultiply(matrix1, matrix2, i, 0, 0, 0, 0);				
				QueryPerformanceCounter((LARGE_INTEGER *)&finish);

				if (finish - start < diff) {
					diff = finish - start;
                }

				//Free memory
				for(int j = 0; j < i; j++)
				{
					free(stupMult[j]);
				}
				free(stupMult);
        }
		printf("Simple time==>%f ms\n", diff * 1000.0 / frequency);
		//diff = ULONG_MAX;

		//SHTRASSEN
		for (size_t t = 0; t < TESTS_NUMBER; t++) {
				QueryPerformanceCounter((LARGE_INTEGER *)&start);
				//shtrasMult = pureShtrassenMultiply(matrix1, matrix2, i, 0, 0, 0, 0);
				shtrasMult = pureShtrassenMultiply(matrix1, matrix2, i, 0, 0, 0, 0);
				QueryPerformanceCounter((LARGE_INTEGER *)&finish);
 
                if (finish - start < diff2) {
                        diff2 = finish - start;
                }

				//Free memory
				for(int j = 0; j < i; j++)
				{
					free(shtrasMult[j]);
				}
				free(shtrasMult);
        }
		printf("SHTRASSEN time==>%f ms\n", diff2 * 1000.0 / frequency);
		printf("win==>%f\n", (double)diff / (double)diff2);
		printSeparator();

		diff = MAXINT64;
		diff2 = MAXINT64;

		// Free generated matrixes.
		for(int j = 0; j < i; j++)
		{
			free(matrix1[j]);
		}
		free(matrix1);
		for(int j = 0; j < i; j++)
		{
			free(matrix2[j]);
		}
		free(matrix2);

		/*if(equals(stupMult, shtrasMult, i))
		{
			printf("%i Results are equal.\n",i);
		}
		else
		{
			printf("%i Results are DIFFERENT!!!\n", i);
		}*/

		/*printf("Stupid:\n");
		printMatrix(stupMult, i);
		printf("SHTRASSEN:\n");
		printMatrix(shtrasMult, i);*/
		//printSeparator();
	}

	/*printf("Stupid:\n");
	printMatrix(stupMult, N);
	printf("SHTrassen:\n");
	printMatrix(shtrasMult, N);*/

	getchar();
	return 0;
}